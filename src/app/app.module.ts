import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { DexieService } from './core/dexie.service';
import { TodosService } from './todos.service';
import { AppTodoListComponent } from './app-todo-list/app-todo-list.component';
import { AppAddTodoComponent } from './app-add-todo/app-add-todo.component';

@NgModule({
  declarations: [
    AppComponent,
    AppTodoListComponent,
    AppAddTodoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ DexieService , TodosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
