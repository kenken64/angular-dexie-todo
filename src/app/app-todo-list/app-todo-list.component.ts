import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { TodoWithID } from '../todos.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './app-todo-list.component.html',
  styleUrls: ['./app-todo-list.component.css']
})
export class AppTodoListComponent implements OnInit {
  @Input() todos: Array<TodoWithID>;
  @Output() toggleTodo = new EventEmitter();
  @Output() deleteTodo = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onTodoToggle(event, id, newValue) {
    event.preventDefault();
    event.stopPropagation();
    this.toggleTodo.emit({
      id,
      done: newValue,
    });
  }

  onDelete(id) {
    this.deleteTodo.emit(id);
  }

}
