import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAddTodoComponent } from './app-add-todo.component';

describe('AppAddTodoComponent', () => {
  let component: AppAddTodoComponent;
  let fixture: ComponentFixture<AppAddTodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppAddTodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAddTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
