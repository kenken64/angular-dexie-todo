import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-todo',
  templateUrl: './app-add-todo.component.html',
  styleUrls: ['./app-add-todo.component.css']
})
export class AppAddTodoComponent implements OnInit {
  @Output() addTodo = new EventEmitter();
  title = '';

  constructor() { }

  ngOnInit() {
  }

  onAddTodo() {
    this.addTodo.emit(this.title);
  }

}
